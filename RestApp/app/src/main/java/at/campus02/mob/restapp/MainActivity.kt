package at.campus02.mob.restapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val model: RestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        model.questiontext.observe(this, Observer {
            question -> questiontext.text = question ?: ""
        })

        model.errortext.observe(this, Observer {
            error -> errortext.text = error ?: ""
        })

        fetchbutton.setOnClickListener{
            model.fetchRandomQuestion()
        }

    }

}
