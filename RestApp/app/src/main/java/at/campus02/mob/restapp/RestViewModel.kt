package at.campus02.mob.restapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RestViewModel : ViewModel() {

    val questiontext:MutableLiveData<String?> = MutableLiveData()

    val errortext:MutableLiveData<String?> = MutableLiveData()

    fun fetchRandomQuestion() {
        // hier erfolgt der zugriff auf die REST schnittstelle

        questiontext.value = "hardcoded Frage, das ist die Frage vom Server"
    }

}