package at.campus02.mob.restapp

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

data class Question(val text: String)

interface QuizGameApi {
    // pfad angeben! relativ von der basis url aus --> {questionid} = parameter
    @GET("rest/questions/{questionid}")
    // das wird der rest api zugriff
    fun question(@Path("questionid") questionid: Int): Deferred<Response<Question>>

}

object RestApi {


    val quiz = Retrofit.Builder()
        .baseUrl("http://quiz.microsoft.com::8000")
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder().build()
            )
        )
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(QuizGameApi::class.java)
}