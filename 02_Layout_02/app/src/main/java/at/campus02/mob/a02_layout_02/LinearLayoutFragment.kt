package at.campus02.mob.a02_layout_02

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_linear_layout.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LinearLayoutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LinearLayoutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_linear_layout, container, false)
    }

    override fun onStart() {
        super.onStart()

        navigationToConstraintButton.text = "Simple Layout"

        // Kotlin Variante für nur diesen Button ein OnClick Button als Lambda Variante
            navigationToConstraintButton.setOnClickListener {
                findNavController().navigate(R.id.action_linearLayoutFragment_to_constraintLayoutFragment)
            }



        }
    }

