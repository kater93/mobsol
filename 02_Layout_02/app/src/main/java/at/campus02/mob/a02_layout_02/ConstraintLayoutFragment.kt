package at.campus02.mob.a02_layout_02

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_constraint_layout.*

/**
 * A simple [Fragment] subclass.
 */
class ConstraintLayoutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_constraint_layout, container, false)
    }



    override fun onStart() {
        super.onStart()

        questionText.text = "Hier kommt der Fragentext hin..."

        answer1Button.text = "Antwort 1"
        answerButton2.text = "Antwort 2"
    }

}
