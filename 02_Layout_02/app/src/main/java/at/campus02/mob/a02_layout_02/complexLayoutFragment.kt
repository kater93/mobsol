package at.campus02.mob.a02_layout_02

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_complex_layout.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [complexLayoutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class complexLayoutFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_complex_layout, container, false)
    }

    override fun onStart() {
        super.onStart()

        question.text = "Wessen Genesung schnell voranschreitet, der erholt sich ..."
        answer1.text = "... hinguckens"
        answer2.text = "... anschauends"
        answer3.text = "... zusehends"
        answer4.text = "... glotzends"
    }

}
