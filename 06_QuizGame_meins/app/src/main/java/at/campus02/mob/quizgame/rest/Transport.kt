package at.campus02.mob.quizgame.rest

data class User(
    val username: String,
    val firstname: String?, // muss nicht unbedingt dabei sein, deshalb ?
    val lastname: String?
)

data class Question(
    val id: Int,
    val text: String,
    val answer1: String,
    val answer2: String,
    val answer3: String,
    val answer4: String,
    val correctchoice: Choice // a b c d --> enum
)

enum class Choice { A, B, C, D, NONE }

// Gamequestion ist erweitert um Antowrt und GameId
data class GameQuestion (
    val id: Int,
    val question: Question,
    val answer: Choice?
    // gameId wird nicht gebraucht
    // nur zur Sicherheit drin für Antworten, doppelt drin, haben in der question auch die id, die wir vom server holen
    // das hier holen wir nicht vom server
/*
    "id": 2,
    "question": {
    "id": 4,
    "text": "Wie heißt Indiens Hauptstadt?",
    "answer1": "Neu-Delhi",
    "answer2": "Mumbai",
    "answer3": "Bangkok",
    "answer4": "Peking",
    "correctchoice": "A"
},
"answer": "A",
"gameId": 1
*/
) {
    // auch möglich als: val isOpen: Boolean get() = answer == null
    val isOpen: Boolean get() {
        if (answer == null) return true
        else return false
    }
}

data class Game(
    val id: Int,
    val questions: List<GameQuestion> = listOf(),
    val player: User
) {
    val hasOpenQuestions: Boolean = get() {
        if (Questions.any {it.isOpen})
            return true
        else
            return false
    }

    // nicht fertig gemacht - siehe mair's repo
}

