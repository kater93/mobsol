package at.campus02.mob.quizgame.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.campus02.mob.quizgame.rest.Choice
import at.campus02.mob.quizgame.rest.Game
import at.campus02.mob.quizgame.rest.GameQuestion
import at.campus02.mob.quizgame.rest.quizApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

enum class State {
    // am Anfang, kein Spiel geladen
    IDLE,
    // während des Ladens
    LOADING,
    // Spiel fertig geladen, es kann gespielt werden
    READY,
    // Ratezeit läuft
    GUESSING,
    // aktuelle Frage beantwortet
    ANSWERED,
    // alle Fragen beantwortet
    FINISHED
}



class QuizViewModel: ViewModel() {

    // ------------------------------------------------------------------------------
    // Properties (UI reagiert darauf über Observer)
    // darauf achten den UI Main Thread nicht zu blockieren
    // ------------------------------------------------------------------------------
    var username: MutableLiveData<String?> = MutableLiveData("kertl")

    val state: MutableLiveData<State> = MutableLiveData(State.IDLE)

    val error: MutableLiveData<String?> = MutableLiveData(null)


    val currentGameQuestion: GameQuestion?
        get() = game?.questions?.getOrNull(index)// wird bei jedem Zugriff ausgewertet

    // 18.06. - CountDown: restlice Zeit (0...100) - für Progressbar
    // auf das remainingGuessingTIme reagieren -> im GameScreenFragment
    var remainingGuessingTime: MutableLiveData<Int> = MutableLiveData(0)
        private set



    // ------------------------------------------------------------------------------
    // Actions (getriggert von einem Event, zB klickt Spieler einen Button
    // ------------------------------------------------------------------------------
    fun startNewGame() {
        // hier wird REST Api aufgerufen
        // in einem eigenen Thread
     //   if (username.value == null) return
        val username = this.username.value ?: return
        // dann unten nur username ohne !!

        state.value = State.LOADING

        // REST calls in einem eigenen hintergrundthread aufrufen, damit
        GlobalScope.launch {
            val response = quizApi.startGameFor(username).await()
        // könnte auch sein dass username nullable ist, deshal rot > vorher überprüfen

            if (response.isSuccessful) {
                val gameFromServer = response.body()
                // jetzt im Hauptthread wieder setzen - neuen thread starten

                // Updates sollten dann wieder im Main-Thread (UI) passieren,
                // MainScope() oder im fall von LiveData über "postValue"
                // App läuft im MainThread (außer man hat zB Timer, asynchrone Aufrufe, die gehören in einen eigenen Thread
                MainScope().launch {
                    game = gameFromServer
                    index = 0
                    state.value = State.READY
                    // gleich mi raten anfangen
                    startGuessing()
                }
            } else {
                error.postValue("Could not create new game (HTTP ${response.code()})")
                state.postValue(State.IDLE)
            }
        }
    }

    // 18.06
    fun startGuessing() {
        state.value = State.GUESSING
        remainingGuessingTime.value = 100
        countdown.start()
    }

    fun chooseAnswer(choice: Choice) {



        state.value = if (game?.hasOpenQuestions == true) State.ANSWERED else State.FINISHED
    }

    fun goToNextQuestion() {
        // Sicherheitsabfrage
        if (state.value != State.ANSWERED) return

        index++
        startGuessing()

        // OLD
  //      if (currentGameQuestion == null) {
        //      state.value = State.FINISHED
  //      } else {
   //         state.value = state.value // state da lassen wo er gerade ist
   //     }
    }

    // ------------------------------------------------------------------------------
    // Private Properties und Hilfsfunktionalität (nicht von außen benutzt)
    // ------------------------------------------------------------------------------
    private var game: Game? = null
    private var index = -1

    // Count Down
    // onject: Klasse draus machen
    private val countdown: CountDownTimer = object : CountDownTimer(10_000, 500) {
        override fun onFinish() {
            remainingGuessingTime.value = 0
        }

        override fun onTick(millisUntilFinished: Long) {
            remainingGuessingTime.value = (millisUntilFinished / 100).toInt()
        }

    }
}