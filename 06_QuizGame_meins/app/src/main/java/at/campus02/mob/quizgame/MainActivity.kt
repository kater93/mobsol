package at.campus02.mob.quizgame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import at.campus02.mob.quizgame.viewmodel.QuizViewModel

class MainActivity : AppCompatActivity() {

    private val model: QuizViewModel by viewModels() //selbes model erwenden, wenn fehler drauf reagieren

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        model.error.observe(this, Observer{ errorMessage ->
            if (!errorMessage.isNullOrEmpty()) {
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
            }
        })
    }
}
