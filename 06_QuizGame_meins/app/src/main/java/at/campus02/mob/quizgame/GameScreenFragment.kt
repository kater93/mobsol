package at.campus02.mob.quizgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import at.campus02.mob.quizgame.viewmodel.QuizViewModel
import at.campus02.mob.quizgame.viewmodel.State
import kotlinx.android.synthetic.main.fragment_game_screen.*

/**
 * A simple [Fragment] subclass.
 */
class GameScreenFragment : Fragment() {

    private val model: QuizViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_screen, container, false)
    }

    override fun onStart() {
        super.onStart()

        model.state.observe(this, Observer {state ->
            when(state) {
            //    State.IDLE, State.LOADING -> hideQuestion()
                State.READY, State.GUESSING, State.ANSWERED -> displayQuestion()
                else -> hideQuestion()
            }
        })

        // observe --> LifecycleTime.. nehmen
        // this braucht er damit er weiß dass er das Lifecycle Objekt instanzieren soll
        model.remainingGuessingTime.observe(this, Observer { remainder ->
            timer.progress = remainder // timer setzen
            if (remainder > 0) {
                timer.visibility = View.VISIBLE
                continueLayout.visibility = View.GONE
            } else {
                timer.visibility = View.GONE
                continueLayout.visibility = View.VISIBLE
            }


        })

        continueLayout.setOnClickListener {
            model.goToNextQuestion()
        }

    }

    //------------------------------------------------------------------------------------
    // Private Hilfsmethoden
    //------------------------------------------------------------------------------------

    fun hideQuestion() {
        questionLayout.visibility = View.GONE
        answerLayout.visibility = View.GONE
    }

    fun displayQuestion() {
        val question = model.currentGameQuestion?.question ?: return

        questionLayout.visibility = View.VISIBLE
        answerLayout.visibility = View.VISIBLE

        questionLabel.text = question.text
        answer1.text = question.answer1
        answer2.text = question.answer2
        answer3.text = question.answer3
        answer4.text = question.answer4

    }
}
