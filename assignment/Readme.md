## Aufgabenstellung

Erweitern Sie die von uns entwickelte "QuizApp" um die Anzeige des Headers und senden Sie bei Beantwortung einer Frage diese an das REST-API.

### Teil 1: User Interface und ViewModel

Im Header der App befinden sich 10 Views (progress1layout... progress10layout). 
Aktuell verwenden diese alle die drawable Ressource "progress_neutral.xml" als Hintergrund.

Diese Views sollen je nach Stand des Spiels einen Hintergrund erhalten, der dem User anzeigt, welche Fragen bereits beantwortet wurden (und ob richtig oder falsch), welche nicht und welche Frage gerade aktiv ist.

Dabei sollte folgende Logik gelten:

* progress1layout entspricht der Frage 1, ..., progress10layout entspricht der Frage 10
* wurde die entsprechende Frage bereits beantwortet, so
    * verwenden Sie "progress_correct.xml" als Hintergrund, wenn die Frage richtig beantwortet wurde
    * oder "progress_wrong.xml" als Hintergrund, wenn die Frage falsch beantwortet wurde
* wurde die entsprechende Frage noch nicht beantwortet, so
    * verwenden Sie "progress_current.xml" als Hintergrund, falls es sich um die aktuell angezeigte Frage handelt
    * oder "progress_neutral.xml" sonst

Außerdem ist ein TextView mit der ID "progresstext" unterhalb der oben genannten 10 Views vorgesehen. In diesem View soll zusätzlich angezeigt werden, die wievielte Frage aktuell angezeigt wird.

* Zeigen Sie im TextView "progresstext" einen Info-Text der Form "Frage 3 / 10" an (3 entspricht dabei der tatsächlichen Frage - Achtung: nicht bei 0 starten, und idealerweise statt "10" die tatsächliche Listengröße verwenden).

Rechts davon befindet sich ein weiterer TextView mit der ID "username"  - in diesem soll der Username aus dem ViewModel angezeigt werden (sofern er halt vorhanden ist).

* Verwenden Sie dazu einen Observer auf dem ViewModel

### Teil 2: Anbindung an das REST API

In diesem Teil der Aufgabenstellung sollen Sie bei Beantwortung einer Frage diese Antwort auch an den Server schicken, damit sie dort in der Datenbank abgespeichert werden kann. 

* Erweitern Sie dazu das Interface QuizApi um eine entsprechende Methode 
    * Finden Sie dazu in der Dokumentation der Schnittstelle [http://quiz.moarsoft.com:8000/](http://quiz.moarsoft.com:8000/) die passende Methode zum Beantworten einer Frage.
    * Annotieren Sie die neue Methode (den Namen können Sie frei wählen) mit der entsprechenden Retrofit-Annotation (GET oder POST) inklusive des entsprechenden Pfads.
    * Falls eine POST-Methode verwendet wird und Sie schicken eine Datenklasse als "Body" des Requests an den Server, so annotieren Sie den entsprechenden Parameter mit @Body (``retrofit.http.Body``, analog zu @PathParam, aber ohne Annotations-Parameter) 
* Verwenden Sie die neu definierte Methode, um bei Beantwortung der Frage die aktuelle ``GameQuestion`` an die REST-Schnittstelle zu senden.
* Sollte der Request nicht erfolgreich abgesetzt werden können, sorgen Sie dafür, dass eine entsprechende Fehlermeldung als Toast angezeigt wird. Die App soll trotzdem normal weiter funktionieren. 

__Anmerkung:__ wie im Kurs besprochen ist dieser Teil optional - für ein "Sehr Gut" sollte er allerdings implementiert werden. 

### Abgabe

Legen Sie Ihr Projekt als zip-Datei in Moodle unter "Abgabe Assignment" ab. (Abgabe ist möglich von Fr. 19.06. 00:00 Uhr bis Fr. 26.06. 08:30). 