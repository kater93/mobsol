package at.campus02.mob.quizgame.rest

data class User(
    val username: String,
    val firstname: String?,
    val lastname: String?
)

// Vom Server kriegen wir bei einem neuen Spiel immer nur "A", "B", "C" oder "D".
// "NONE" sehen wir vor, weil wir diesen Enum auch für die Spieler-Antwort verwenden, und
// "NONE" bei Ablauf der Ratezeit verwendet wird.
enum class Choice { A, B, C, D, NONE }

data class Question(
    val id:Int,
    val text: String,
    val answer1: String,
    val answer2: String,
    val answer3: String,
    val answer4: String,
    val correctchoice: Choice
)


data class GameQuestion(
    val id: Int, 
    val question: Question,
    var answer: Choice?,
    // wird nicht gebraucht, deshalb weggelassen, kann man aber natürlich auch verwenden
    val gameId: Int
) {
    // auch möglich als: val isOpen: Boolean get() = answer == null
    val isOpen: Boolean get() {
        return answer == null
    }

    val isCorrect: Boolean get() {
        return answer == question.correctchoice
    }
}

data class Game(
    val id: Int,
    val questions: List<GameQuestion> = listOf(),
    val player: User
) {
    // auch möglich als: val hasOpenQuestions: Boolean get() = questions.any { it.isOpen }
    val hasOpenQuestions: Boolean get() {
        return questions.any { question -> question.isOpen }
    }
}

