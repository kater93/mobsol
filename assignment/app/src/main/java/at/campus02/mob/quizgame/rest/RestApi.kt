package at.campus02.mob.quizgame.rest

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface QuizApi {

    @POST("rest/games/{username}")
    fun startGameFor(@Path("username") username: String): Deferred<Response<Game>>

    // @Headers("Content-Type: application/json")
    @POST("/rest/games/{gameId}/answer")
    fun setQuestionAnswered(@Path("gameId") gameId: Int, @Body currentQuestion: GameQuestion?) : Deferred<Response<Unit>> // unit == void

}

val quizApi = Retrofit.Builder()
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(
        MoshiConverterFactory.create(

            Moshi.Builder().build()

        )
    )
    .baseUrl("http://quiz.moarsoft.com:8000/")
    .build()
    .create(QuizApi::class.java)



// POST /rest/games/($gameId)/answer
// 200 OK � erfolgreich
// 404 Not Found � falls das Spiel mit der angegebenen gameid nicht existiert oder die SpielFrage mit der angegebenen id nicht existiert
// 400 Bad Request � Falls die gameId (Pfadparameter) nicht mit der ID des Spiels der GameQuestion �bereinstimmt oder die SpielFrage bereits beantwortet war oder die Antwort des Benutzers nicht angegeben wurde.

