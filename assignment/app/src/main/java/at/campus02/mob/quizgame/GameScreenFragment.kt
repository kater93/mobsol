package at.campus02.mob.quizgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.rangeTo
import androidx.core.view.get
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import at.campus02.mob.quizgame.rest.Choice
import at.campus02.mob.quizgame.rest.GameQuestion
import at.campus02.mob.quizgame.viewmodel.QuizViewModel
import at.campus02.mob.quizgame.viewmodel.State
import kotlinx.android.synthetic.main.fragment_game_screen.*

/**
 * A simple [Fragment] subclass.
 */
class GameScreenFragment : Fragment() {

    private val model: QuizViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_screen, container, false)
    }

    override fun onStart() {
        super.onStart()

        model.username.observe(this, Observer {
                newUsername -> username.text = newUsername?.toString()
        })

        model.progresstext.observe(this, Observer {
            ptext -> progresstext.text = ptext?.toString()
        })


        // -----------------------------------------------------------------------------------------
        // UI anpassen (Observer)
        // -----------------------------------------------------------------------------------------
        model.state.observe(this, Observer { state ->
            when(state) {
                State.READY, State.GUESSING, State.ANSWERED, State.FINISHED -> displayQuestion()
                else -> hideQuestion()
            }
        })

        model.remainingGuessingTime.observe(this, Observer {  remainder ->
            // progress setzen auf ProgressBar (0..100)
            timer.progress = remainder
            if (remainder > 0) {
                timer.visibility = View.VISIBLE
                continueLayout.visibility = View.GONE
            } else {
                timer.visibility = View.GONE
                continueLayout.visibility = View.VISIBLE
            }
        })



        // -----------------------------------------------------------------------------------------
        // AKTIONEN
        // -----------------------------------------------------------------------------------------
        continueLayout.setOnClickListener {
            model.goToNextQuestion()

        }

        answer1Layout.setOnClickListener {
            model.chooseAnswer(Choice.A)
        }
        answer2Layout.setOnClickListener {
            model.chooseAnswer(Choice.B)
        }
        answer3Layout.setOnClickListener {
            model.chooseAnswer(Choice.C)
        }
        answer4Layout.setOnClickListener {
            model.chooseAnswer(Choice.D)
        }

    }

    // ---------------------------------------------------------------------------------------------
    // Private Hilfsmethoden
    // ---------------------------------------------------------------------------------------------

    fun hideQuestion() {
        questionLayout.visibility = View.GONE
        answerLayout.visibility = View.GONE
    }

    fun displayQuestion() {
        val question = model.currentGameQuestion?.question ?: return

        questionLayout.visibility = View.VISIBLE
        answerLayout.visibility = View.VISIBLE

        questionLabel.text = question.text
        answer1.text = question.answer1
        answer2.text = question.answer2
        answer3.text = question.answer3
        answer4.text = question.answer4

        giveFeedbackOnButtons()
        giveFeedbackInHeader()
    }

    private fun giveFeedbackOnButtons() {
        val gameQuestion = model.currentGameQuestion ?: return

        answer1Layout.setBackgroundResource(resourceFor(gameQuestion, Choice.A))
        answer2Layout.setBackgroundResource(resourceFor(gameQuestion, Choice.B))
        answer3Layout.setBackgroundResource(resourceFor(gameQuestion, Choice.C))
        answer4Layout.setBackgroundResource(resourceFor(gameQuestion, Choice.D))
    }


    private fun giveFeedbackInHeader() {
        val gameQuestion = model.currentGameQuestion ?: return
        val listProgressLayouts = list_item_progress
        var counter = 0;

        for (i in model.listOfQuestions?.indices!!) {
            val question = model.listOfQuestions!![i]
            val currentLayout = listProgressLayouts[i]
            counter++
            if (model.currentGameQuestion === question) {
                currentLayout.setBackgroundResource(R.drawable.progress_current)
                model.progresstext.value = "Frage " + counter.toString() + " / " + model.listOfQuestions!!.size
            //    val q = model.listOfQuestions!![i]


                if (gameQuestion.isCorrect) {
                    currentLayout.setBackgroundResource(R.drawable.progress_correct)
                }
                else if (!gameQuestion.isCorrect && !model.currentGameQuestion!!.isOpen) {
                    currentLayout.setBackgroundResource(R.drawable.progress_wrong)
                }
                else {
                    R.drawable.progress_neutral
                }
            }
        }

    }

    private fun resourceFor(gameQuestion: GameQuestion, choice: Choice): Int {
        return when {
            gameQuestion.isOpen -> R.drawable.answer_neutral
            gameQuestion.isCorrect && choice == gameQuestion.answer -> R.drawable.answer_correct
            !gameQuestion.isCorrect && choice == gameQuestion.answer -> R.drawable.answer_wrong
            choice == gameQuestion.question.correctchoice -> R.drawable.answer_hint
            else -> R.drawable.answer_neutral
        }
    }
}
