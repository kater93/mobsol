package at.campus02.mob.quizgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import at.campus02.mob.quizgame.viewmodel.QuizViewModel
import at.campus02.mob.quizgame.viewmodel.State
import kotlinx.android.synthetic.main.fragment_start_screen.*

/**
 * A simple [Fragment] subclass.
 */
class StartScreenFragment : Fragment() {

    private val model: QuizViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_start_screen, container, false)
    }

    override fun onStart() {
        super.onStart()

        // -----------------------------------------------------------------------------------------
        // UI anpassen (Observer)
        // -----------------------------------------------------------------------------------------
        model.state.observe(this, Observer { state ->
            if (state == State.GUESSING) {
                // sicherstellen, dass wir uns gerade auf dem StartScreenFragment befinden,
                // bevor die Navigation ausgelöst wird (ansonsten könnte die APP abstürzen)
                if (findNavController().currentDestination?.label == "fragment_start_screen")
                    findNavController().navigate(R.id.action_startScreenFragment_to_gameScreenFragment)
            }
        })

        // -----------------------------------------------------------------------------------------
        // AKTIONEN
        // -----------------------------------------------------------------------------------------
        startButtonLayout.setOnClickListener {
            model.startNewGame()
        }

    }
}
