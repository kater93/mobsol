package at.campus02.mob.quizgame.viewmodel

import android.os.CountDownTimer
import android.text.Layout
import androidx.core.util.rangeTo
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.campus02.mob.quizgame.rest.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

enum class State {
    // am Anfang, kein Spiel geladen
    IDLE,
    // während des Ladens
    LOADING,
    // Spiel fertig geladen, es kann gespielt werden
    READY,
    // Ratezeit läuft
    GUESSING,
    // aktuelle Frage beantwortet
    ANSWERED,
    // alle Fragen beantwortet
    FINISHED
}

class QuizViewModel: ViewModel() {

    // ---------------------------------------------------------------------------------------------
    // Properties (UI reagiert darauf über Observer)
    // ---------------------------------------------------------------------------------------------
    var username: MutableLiveData<String?> = MutableLiveData("kertl")

    val state: MutableLiveData<State> = MutableLiveData(State.IDLE)

    val error: MutableLiveData<String?> = MutableLiveData(null)

    var progresstext:  MutableLiveData<String?> = MutableLiveData(null)

    var progressheader: MutableLiveData<String?> = MutableLiveData(null)

    val listOfQuestions:  List<GameQuestion>?
        get() = game?.questions?.toList()

    val gameID : Int
        get() = game!!.id


    // die Variante mit "get() = " wertet den Ausdruck bei jedem Zugriff aus, der Wert wird also
    // bei jedem Zugriff neu berechnet.
    val currentGameQuestion: GameQuestion?
        get() = game?.questions?.getOrNull(index)

    // CountDown: restliche Zeit (0..100)
    var remainingGuessingTime: MutableLiveData<Int> = MutableLiveData(0)
        private set


    // ---------------------------------------------------------------------------------------------
    // Actions (getriggert von einem Event, zB. klickt der Spieler einen Button)
    // ---------------------------------------------------------------------------------------------
    fun startNewGame() {

        val username = this.username.value ?: return

        state.value = State.LOADING

        // REST calls in einem eigenen Hintergrund-Thread aufrufen, damit das UI während
        // des Serverzugriffs nicht blockiert.
        GlobalScope.launch {

            val response = quizApi.startGameFor(username).await()
            if (response.isSuccessful) {
                val gameFromServer = response.body()

                // Updates sollten dann wieder im Main-Thread (UI) passieren, daher über
                // MainScope() oder im Fall von LiveData über "postValue"
                MainScope().launch {
                    game = gameFromServer
                    index = 0
                    state.value = State.READY
                    startGuessing()
                }
            } else {
                error.postValue("Could not create new game (HTTP ${response.code()})")
                state.postValue(State.IDLE)
            }
        }

    }

    fun startGuessing() {
        state.value = State.GUESSING
        remainingGuessingTime.value = 100
        countdown.start()

    }

    fun chooseAnswer(choice: Choice) {

        // Falls die Antwort vom Benutzer kommt, müssen wir den CountDown stoppen
        countdown.cancel()
        remainingGuessingTime.value = 0

        // Antwort übernehmen


        if (currentGameQuestion?.isOpen!!) {
            currentGameQuestion?.answer = choice
        }



        // State setzen, je nachdem, ob es noch offene Fragen gibt oder nicht.
        state.value = if (game?.hasOpenQuestions == true) State.ANSWERED else State.FINISHED

        PostFunction(currentGameQuestion)
    }

    fun goToNextQuestion() {
        if (state.value !== State.ANSWERED) return

        index++
        startGuessing()
    }

    fun progressOfQuestions() {

      /*       if (state.value === State.GUESSING) {
            for (q in ListOfQuestions) {
            progresstext.value = "Frage " + listOfQuestions!![index].toString() + " / " + listOfQuestions!!.size.toString()
            }
        } */

  /*
      val rangeBegin =  listOfQuestions?.get(2)


      var rangeEnd = listOfQuestions?.size

      var range = 2..rangeEnd!!

      for (i in range) {
          if (state.value === State.ANSWERED)
              progresstext.value = i.toString()

      }

      for (q in this!!.listOfQuestions!!) {
          progresstext.value = "Frage " + listOfQuestions!![index].toString() + " / " + listOfQuestions!!.size.toString()
      }

      products.forEachIndexed{ index, product -> }


      listOfQuestions?.forEachIndexed { index, gameQuestion ->
          if (state.value === State.ANSWERED)
              progresstext.value = index.toString()
      }



       fun listIterator(index: Int): ListIterator<Int> {
           progresstext.postValue(listOfQuestions!![index].toString())
           if (state.value == State.GUESSING)
           listIterator(index).nextIndex()
           return listIterator(index).nextIndex()
       }   */
  }




// ---------------------------------------------------------------------------------------------
    // Private Properties und Hilfsfunktionalität (nicht von außen benutzt)
    // ---------------------------------------------------------------------------------------------
    private var game: Game? = null
    private var index: Int = -1

    private val countdown : CountDownTimer = object : CountDownTimer(10_000, 500) {
        override fun onFinish() {
            remainingGuessingTime.value = 0
            chooseAnswer(Choice.NONE)
        }

        override fun onTick(millisUntilFinished: Long) {
            remainingGuessingTime.value = (millisUntilFinished / 100).toInt()
        }

    }


    fun PostFunction(currentGameQuestion: GameQuestion?) {

        GlobalScope.launch {
            val response = quizApi.setQuestionAnswered(gameID, currentGameQuestion).await()

            val res = response.code()
            if (!response.isSuccessful) {
                error.postValue("Error")
            }
        }
    }

}