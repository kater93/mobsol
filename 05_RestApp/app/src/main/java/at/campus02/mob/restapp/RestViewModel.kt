package at.campus02.mob.restapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import kotlin.random.Random

class RestViewModel: ViewModel() {

    val questiontext: MutableLiveData<String?> = MutableLiveData()

    val errortext: MutableLiveData<String?> = MutableLiveData()

    fun fetchRandomQuestion() {

        // In einem eigenen Thread starten, am einfachsten über GlobalScope.launch
        // In Android sind "lange" dauernde Funktionsaufrufe nicht im UI-Thread erlaubt.
        GlobalScope.launch {

            // zufällig eine Frage über deren technische ID auswählen
            val randomIndex = Random.nextInt(1, 100)
            val deferredResponse: Deferred<Response<Question>> = quizGameApi.question(randomIndex)

            // "warten" auf Response vom Server
            val response: Response<Question> = deferredResponse.await()

            if (response.isSuccessful) {
                val questions = response.body()

                if (questions == null || questions.isEmpty()) {
                    errortext.postValue("Keine Fragen vom Server!")
                    questiontext.postValue("")
                } else {
                    val randomIndex = Random.nextInt(0, questions.size - 1)
                    val question = questions[randomIndex]



                // questiontext.value = question?.text funktioniert nicht, weil wir in zwei
                // verschiedenen Threads darauf zugreifen (hier und im UI Thread).
                // Bei Verwendung von LiveData funktioniert das "out-of-the-box" über
                // livedata.postValue()
                questiontext.postValue(question.text)
                }

            } else {
                errortext.postValue("Fehler: ${response.code()}")

                // wenn ein Fehler auftritt, können wir auch den Fragentext zurücksetzen
                questiontext.postValue("")
            }

        }

        // UI-Thread wird durch den asynchronen Aufruf oben nicht blockiert, deshalb kommt
        // die Zeile unten noch vor der Server-Antwort
       // questiontext.value = "Warten auf Server-Antwort....."



        // etwaigen vorherigen Fehlertext zurücksetzen
        errortext.value = ""

    }

}