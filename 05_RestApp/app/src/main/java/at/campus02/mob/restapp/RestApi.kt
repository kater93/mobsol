package at.campus02.mob.restapp

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

// Für das automatische Konvertieren von JSON in diese Datenklasse...
data class Question(val text: String)

// Definieren des Interface für die REST-Schnittstelle,
// wir brauchen nur eine Methode für die Abfrage einer einzelnen Question
interface QuizGameApi {

    // HTTP Methode und Pfad (inklusive Pfad-Parametern) über Annotation
    @GET("rest/questions/{questionid}")
    //           Annotation für Retrofit, dadurch wird der PfadParameter mit dem
    //           Methoden-Parameter ersetzt.
    fun question(@Path("questionid") questionid: Int): Deferred<Response<Question>>

    @GET("/rest/questions")
    fun questions(@Path("{questionid}") question: Question): Deferred<Response<List<Question>>>
}

// Retrofit konfigurieren
private val retrofit: Retrofit =
    // Konfigurieren von Retrofit mit dem "Builder"
    Retrofit.Builder()
        // Basis URL setzen
        .baseUrl("http://quiz.moarsoft.com:8000")
        // JSON Konverter einrichten
        .addConverterFactory(
            // wir verwenden Moshi (GSON gäbe es alternativ auch)
            MoshiConverterFactory.create(
                // auch Moshi verwendet einen Builder für die Konfiguration
                Moshi.Builder().build()
            )
        )
        // um Kotlin coroutines verwenden zu können statt Callbacks
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        // "Bauen" der Konfiguration
        .build()

// Erzeugen der Implementierung von QuizGameApi
// (Retrofit definitert eine Klasse, die unser Interface implementiert und die oben erstellte
// Konfiguration für den REST-Zugriff verwendet und gibt uns eine Instanz dieser Klasse zurück).
val quizGameApi = retrofit.create(QuizGameApi::class.java)



