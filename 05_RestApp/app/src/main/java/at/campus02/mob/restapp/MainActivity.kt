package at.campus02.mob.restapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val model: RestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        // UI reagiert auf Änderungen im ViewModel -> mittels Observer
        model.questiontext.observe(this, Observer { question ->
            questiontext.text = question ?: ""
        })
        model.errortext.observe(this, Observer { error ->
            errortext.text = error ?: ""
        })

        // User-Aktionen ändern das ViewModel (meistens OnclickListener)
        fetchbutton.setOnClickListener {
            model.fetchRandomQuestion()
        }
    }

}
