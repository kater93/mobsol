package at.campus02.mob.viewmodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random



class MainActivity : AppCompatActivity() {
/* // alte Variante ohne verwendung des jetpack viewmodels
    // zahl ist zu beginn noch nicht da, deshalb null; das ist das "view model";
    private var model: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        model = savedInstanceState?.getInt("MODEL")
    }

   override fun onSaveInstanceState(outState: Bundle) {
        //ins Bundle ablegen
        if (model != null) {
            outState.putInt("MODEL", model!!) // !! = ich will das trotzdem und bin mir sicher, dass das model da und in der zwischenzeit nicht null geworden ist
        }

        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        adaptUiToModel()

        randomButton.setOnClickListener{
            model = Random.nextInt(1, 10000)
            adaptUiToModel()
            }
        }

      private fun adaptUiToModel() {
        if (model != null) {
            // text setzen, toString weil model ist Int
            numberView.text = model.toString()
        } else {
            numberView.text = "kein Modell"
        }
    }
 */

    // ViewModel verwenden
    private val viewModel: NumberViewModel by viewModels() // by viewModels geht nicht --> library holen --> in build.gradle dependencies "implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'"
// hier kommt ein numberViewModel im Kontext der Activity (bekommt man automatisch) --> viewModel ist eine Instanz dieser Klasse, bei der Number null ist
    // by = lazyInitializor --> hier wird das geholt

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onStart() {
        super.onStart()

        // mit LiveData
        // immer wenn activity da ist, wird sie vom model benachrichtigt (auch beim ersten start)
        viewModel.number.observe(this, Observer{
            adaptUiToModel()
        })

       // ohne LiveData
       // adaptUiToModel()

        randomButton.setOnClickListener{
           // für Variante ohne ViewModel
           // model = Random.nextInt(1, 10000)
            viewModel.number.value = Random.nextInt(0, 10000)

            // ohne LiveData
           //  adaptUiToModel()
            }
        }

    private fun adaptUiToModel() {
      //  if (viewModel.number != null) {
        // mit LiveData muss man value hinzufügen
          if (viewModel.number.value != null) {
            // text setzen, toString weil model ist Int
            numberView.text = viewModel.number.value.toString()
        } else {
            numberView.text = "kein Modell"
        }
    }
}



