package at.campus02.mob.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NumberViewModel : ViewModel() {
    // ist in Klasse "eingewickelt"
    // var number: Int? = null  // ViewModel mit adaptUiToModel

    // um auf änderungen automatisch reagieren zu können, soll er mutable = beobachtbar sein
    var number: MutableLiveData<Int?> = MutableLiveData(null)

}