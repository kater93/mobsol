package at.campus02.mob.quizgame.rest

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.POST
import retrofit2.http.Path

// hier das Interface definieren (bei Retrofit)

interface QuizApi {

    @POST("rest/games/{username}")
    fun startGameFor(@Path("username")username: String): Deferred<Response<Game>>

}

val quizApi = Retrofit.Builder()
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(
        MoshiConverterFactory.create(
            Moshi.Builder().build()
        )
    )
    .baseUrl("http://quiz.moarsoft.com:8000/")
    .build()
    .create(QuizApi::class.java)

