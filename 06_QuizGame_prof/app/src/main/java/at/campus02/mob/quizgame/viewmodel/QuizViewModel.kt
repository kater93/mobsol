package at.campus02.mob.quizgame.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.campus02.mob.quizgame.rest.Game
import at.campus02.mob.quizgame.rest.quizApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class QuizViewModel: ViewModel() {

    // ------------------------------------------------------------------------------
    // Properties (UI reagiert darauf über Observer)
    // darauf achten den UI Main Thread nicht zu blockieren
    // ------------------------------------------------------------------------------
    var username: MutableLiveData<String?> = MutableLiveData("kertl")


    // ------------------------------------------------------------------------------
    // Actions (getriggert von einem Event, zB klickt Spieler einen Button
    // ------------------------------------------------------------------------------
    fun startNewGame() {
        // hier wird REST Api aufgerufen
        // in einem eigenen Thread
        if (username.value == null) return
        // oder val username = this.username.value ?: return
        // dann unten nur username ohne !!

        // REST calls in einem eigenen hintergrundthread aufrufen, damit
        GlobalScope.launch {
            val response = quizApi.startGameFor(username.value!!).await()
        // könnte auch sein dass username nullable ist, deshal rot > vorher überprüfen

            if (response.isSuccessful) {
                val gameFromServer = response.body()
                // jetzt im Hauptthread wieder setzen - neuen thread starten

                // Updates sollten dann wieder im Main-Thread (UI) passieren,
                // MainScope() oder im fall von LiveData über "postValue"
                // App läuft im MainThread (außer man hat zB Timer, asynchrone Aufrufe, die gehören in einen eigenen Thread
                MainScope().launch {
                    game = gameFromServer
                }
            }
        }
    }

    // ------------------------------------------------------------------------------
    // Private Properties und Hilfsfunktionalität (nicht von außen benutzt)
    // ------------------------------------------------------------------------------
    private var game: Game? = null

}